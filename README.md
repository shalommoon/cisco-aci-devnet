# ACI Network as Code Demo
Fork this repo to provide the necessary files to run this demo.
The Demo is meant to run from the staging branch. A webhook to an Ansible Automation Platform is also required to provide a push trigger.

## Install AAP 2.x (Free Trial)
https://www.ansible.com/products/automation-platform

## controller.yml
This playbook can be modified to configure your Ansible Automation Platform Controller "Tower" for the demo

## Devnet ACI Sandbox
 apic_host: sandboxapicdc.cisco.com
 apic_password: '!v3G@!4@Y'

 https://sandboxapicdc.cisco.com



